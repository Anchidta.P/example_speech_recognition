#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding("utf-8")

import speech_recognition as sr
import rospy
from std_msgs.msg import String

# Record Audio
r = sr.Recognizer()
m = sr.Microphone()

pub = rospy.Publisher('TalktoROS', String, queue_size=10)
rospy.init_node('SpeechTalker', anonymous=True)
rate = rospy.Rate(1) # 1hz

#set threhold level
print(' Quiet Please ')
with m as source: r.adjust_for_ambient_noise(source)
print("Set minimum energy threshold to {}".format(r.energy_threshold))

# Speech recognition using Google Speech Recognition
while (True):
    
    print("Say something!")
    with sr.Microphone() as source:
        audio = r.listen(source, phrase_time_limit=3)
        
    try:
        # for testing purposes, we're just using the default API key
        # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
        # instead of `r.recognize_google(audio)`
        text_recognized = r.recognize_google(audio,  language='th-TH')
        print("Google Speech Recognition >> you said >>  " + text_recognized)
        if ( 'สวัสดี' in text_recognized ):
            print("สวัสดี")      
        else:
            print("ฉันไม่รู้จักคำนี้")
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
        

while not rospy.is_shutdown():
    rospy.loginfo(hello_str)
    pub.publish(hello_str)
    rate.sleep()


